﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreWithDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NarutoController : ControllerBase
    {
        // GET 
        [HttpGet]
        public ActionResult<IEnumerable<string>> GetCharacters()
        {
            return new string[] { "Naruto", "Kakashi","Sasuke","Sakura" };
        }
    }
}
