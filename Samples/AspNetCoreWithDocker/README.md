# Asp.Net Core with Docker

## Create the image

    $ docker build -t apinetcorewithdocker .
    
## Run

    $ docker run --rm -p 2000:80 --name apinetcorewithdocker apinetcorewithdocker:latest

> Open in browser: http://localhost:2000/api/naruto
