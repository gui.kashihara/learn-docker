
# Learn Docker

## What is docker?
Docker is a platform for developers and sysadmins to develop, deploy, and run applications with containers.

**Benefits of Docker:**

 - Consistent development environments for the entire team. All developers use the same OS, same system libraries, same language runtime, independent of the host OS;
 - With application development using Docker, you don't have to install multiple programs on your system;
 - Deployment is easy. If it runs in your container, it will run on your server just the same;

## Architecture

![Arquitetura Docker](https://docs.docker.com/engine/images/architecture.svg)
https://docs.docker.com/engine/docker-overview/#docker-architecture

## Commands

**Images**

 - List
	 - `docker images`
 - Search
	 - `docker search (parametro)`
 - Add 
	 - `docker pull (parametro)`
 - Delete
	 - `docker rmi (name/id)`
 - Delete all Images
	 - `docker rmi $(docker images -q)`
 - Create
	 - `docker build -t name .`


**Containers**

 - List
	 - `docker ps` 
 - List all
	 - `docker ps -a`
 - Run
	 - `docker run (name image)`
 - Remove
	 - `docker rm (name/id)`
 - Remove all
	 - `docker rm $(docker ps -qa)`
 - Display a live stream of container(s) resource usage statistics
	 - `docker stats (name/id)` 

